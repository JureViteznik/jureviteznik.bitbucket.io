
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';


var username = "ois.seminar";
var password = "ois4fri";

/*
var username = 'guidemo';
var password = 'gui?!demo123';
*/

var apiKey ="k34ws9zjvvvzc4etxd3k29hv";
var appendApiKeyHeader = function( xhr ) {
  xhr.setRequestHeader('Api-Key', apiKey)
};
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 function generirajTriOsebe(){
     console.log("ayyyyyyy dela");
     var ehrId1 = generirajPodatke(1);
     var ehrId2 = generirajPodatke(2);
     var ehrId3 = generirajPodatke(3);
     
         $("#generirajPodatke").append("<br/><div class='obvestilo label label-success fade-in'><b>ehrId1:</b> " + ehrId1 + "</div>");
         $("#generirajPodatke").append("<br/><div class='obvestilo label label-success fade-in'><b>ehrId2:</b> " + ehrId2 + "</div>");
         $("#generirajPodatke").append("<br/><div class='obvestilo label label-success fade-in'><b>ehrId3:</b> " + ehrId3 + "</div>");
     
 } 
 
function generirajPodatke(stPacienta) {
    var ehrId = "";
    
    sessionId = getSessionId();
    
	var ime = "";
	var priimek = "";
    var datumRojstva ="";
    
    switch (stPacienta) {
        case 1:
            ime = "Mojca";
            priimek = "Tiran";
            datumRojstva = "1981-08-06";
            break;
        case 2:
            ime = "Joze";
            priimek = "Smrdelj";
            datumRojstva = "1969-04-18";
            break;
        case 3:
            ime = "Janez";
            priimek = "Novak";
            datumRojstva = "1990-01-01";
            break
        default:
            
    }

	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    async: false,
	    type: 'POST',
	    success: function (data) {
	        ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: datumRojstva,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {},
	            error: function(err) {
	                console.log("kreten si");
	            }
	        });
	    }
	});
	 var arrayDatumMeritve;
        var arrayVisina;
        var arrayTeze;
	if(stPacienta == 1){
        arrayDatumMeritve = ["1981-08-01", "1981-12-06", "1982-08-06", "1987-09-01","1991-11-18","1996-02-08","2001-08-08","2011-10-16","2015-08-06","2018-01-05"];
        arrayVisina = ["51", "56", "75", "120", "131", "156", "164", "165", "165", "167"];
        arrayTeze = ["2.6", "7", "8", "19", "25", "43", "51", "53", "52", "50"];
    }else if(stPacienta == 2){
        arrayDatumMeritve = ["1969-04-18", "1979-06-05", "1980-09-23", "1984-04-18","1989-08-12","1996-11-30","2002-11-02","2009-04-18","2016-03-08","2018-04-18"];
        arrayVisina = ["48", "144", "153", "160", "175", "180", "180", "181", "181", "181"];
        arrayTeze = ["2", "40", "47", "56", "69", "73", "76", "79", "80", "79"];
    }else if(stPacienta == 3){
        arrayDatumMeritve = ["1998-01-01", "2001-03-06", "2010-05-10", "2011-07-13","2012-01-01","2013-02-26","2014-04-03","2015-05-18","2016-06-11","2017-01-20"];
        arrayVisina = ["124", "145", "177", "178", "178", "180", "181", "181", "182", "182"];
        arrayTeze = ["31", "37", "80", "85", "87", "94", "98", "101", "97", "105"];
    }
    
    
    for(var i = 0; i < 10; i++){
        var datumInUra = arrayDatumMeritve[i];
    	var telesnaVisina = arrayVisina[i];
    	var telesnaTeza = arrayTeze[i];
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    async: false,
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		      
		    },
		    error: function(err) {
		    }
		});
    	
    }
  // TODO: Potrebno implementirati
    console.log(ehrId + "");
  return ehrId;
}

var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var visina;
var teza;


function izracunajBMI() {
    
    //PRVO ZBRISI GRAFA IN SLIKO
    $("#tleJeSlika").html("");
    $("#graf_visine").html("");
    $("#graf_teze").html("");
    
    
    console.log("WELL TU DELA");
	sessionId = getSessionId();
    
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
	    
	    $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Zadnje meritve " +
          " osebe <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
			
			$.when(ajax1(ehrId),ajax2(ehrId)).done(function(a1){
            // the code here will be executed when all four ajax requests resolve.
            // a1, a2, a3 and a4 are lists of length 3 containing the response text,
            // status, and jqXHR object for each of the four ajax calls respectively.
                var BMI = (teza/(visina * visina)).toFixed(2);
            	console.log(teza + " " + visina);
            	console.log(BMI + "");
            	
            	var results = "<table class='table table-striped " +
                "table-hover'>";
        			        
        		results += "<tr><td class='text-left'>BMI zadnjih meritev</td>" +
                     "<td class='text-right'>" + BMI + "</td></tr>";
        			        
        	    results += "</table>";
        	    $("#rezultatMeritveVitalnihZnakov").append(results);
        	    
        	    var searchRequest;
        	    
        	    var zacetniURL = "https://api.gettyimages.com/v3/search/videos?fields=id,title,thumb,comp&phrase=";
        	    var napis = "";
        	    if(BMI < 18.5){
        	        //CE UPORABLAS UNDERWEIGHT DOBIS VECINA SLIKE CRNIH OTROK KAR PA NI LIH TISTO KAR RABBMO, JE PA ZANIMIVO!
        	        //ZATO SEM RAJE UPORABIL FRAZO "very slim body"
        	      searchRequest  = { "phrase": "very slim body"};
        	     
        	      if(BMI < 16){
        	          napis = "Severely underweight";
        	      }else{
        	          napis = "Underweight";
        	      }
        	    }else if(BMI < 25){
        	        searchRequest  = { "phrase": "healthy body"};
        	        napis = "Healthy weight"
        	    }else{
        	        searchRequest  = { "phrase": "overweight"};
        	        if(BMI < 30){
        	            napis = "Overweight";
        	        }else{
        	            napis = "Obese";
        	        }
        	    }
        	    
        	    
        	      $.ajax({
                    type: "GET",
                    beforeSend:appendApiKeyHeader,
                    url: "https://api.gettyimages.com/v3/search/images",
                    data: searchRequest
                  }).success(function (data, textStatus, jqXHR) { 
                
                    console.log(data.images[0].display_sizes[0].uri);
                    var neki = Math.floor(Math.random() * 10); 
                    var results1 = "<img src=" + data.images[neki].display_sizes[0].uri + " width='50%' height='50%' >";
                    results1 += "<div style='font-family:verdana;'>" + napis + "</div>";
                     $("#tleJeSlika").append(results1);
                  }).fail(function (data, err) {
                
                    console.log(data); 
                
                  });
        	    
        	    
        	    
            });
			
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	    
		$.ajax({
            url: baseUrl + "/view/" + ehrId + "/weight",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (res) {
                //display newest
                var bt = res[0].weight + " " + res[0].unit;
                $('.last-bt').text(bt);
                $('.last-bt-date').text(res[0].time);

                res.forEach(function (el, i, arr) {
                    var date = new Date(el.time);
                    el.date = date.getDate() + '-' + monthNames[date.getMonth()];
                });
                console.log(res);
                
                var data = res,
                    config = {
                      data: data,
                      xkey: 'time',
                      ykeys: ['weight'],
                      labels: ['weight'],
                      fillOpacity: 0.6,
                      hideHover: 'auto',
                      behaveLikeLine: true,
                      resize: true,
                      pointFillColors:['#ffffff'],
                      pointStrokeColors: ['black'],
                      lineColors:['red']
                  };
              
                 config.element = 'graf_teze';
                Morris.Line(config);
            }
        });
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (res) {
                //display newest
                var bt = res[0].height + " " + res[0].unit;
                $('.last-bt').text(bt);
                $('.last-bt-date').text(res[0].time);

                res.forEach(function (el, i, arr) {
                    var date = new Date(el.time);
                    el.date = date.getDate() + '-' + monthNames[date.getMonth()];
                });
                console.log(res);
                
                var data = res,
                    config = {
                      data: data,
                      xkey: 'time',
                      ykeys: ['height'],
                      labels: ['height'],
                      fillOpacity: 0.6,
                      hideHover: 'auto',
                      behaveLikeLine: true,
                      resize: true,
                      pointFillColors:['#ffffff'],
                      pointStrokeColors: ['black'],
                      lineColors:['blue']
                  };
              
                 config.element = 'graf_visine';
                Morris.Line(config);
            }
        });
	}
	

}

function ajax1(ehrId) {
    // NOTE:  This function must return the value 
    //        from calling the $.ajax() method.
    return $.ajax({
          		    url: baseUrl + "/view/" + ehrId + "/" + "height",
        		    type: 'GET',
        		    headers: {"Ehr-Session": sessionId},
        		    success: function (res) {
        		    	if (res.length > 0) {
        		    	 visina = res[0].height/100;
        		    	    
        			    	var results = "<table class='table table-striped " +
                "table-hover'>";
        			        
        			results += "<tr><td class='text-left'>Visina</td><td class='text-middle'>" + res[0].height +
                      " " + res[0].unit + "</td><td class='text-right'>" + res[0].time +
                      "</td></tr>";
        			        
        			        results += "</table>";
        			        $("#rezultatMeritveVitalnihZnakov").append(results);
        		    	} else {
        		    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-warning fade-in'>" +
                "Ni podatkov!</span>");
        		    	}
        		    },
        		    error: function() {
        		    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
        		    }
        		});
		
}

function ajax2(ehrId) {
    // NOTE:  This function must return the value 
    //        from calling the $.ajax() method.
    return $.ajax({
          		    url: baseUrl + "/view/" + ehrId + "/" + "weight",
        		    type: 'GET',
        		    headers: {"Ehr-Session": sessionId},
        		    success: function (res) {
        		    	if (res.length > 0) {
        		    	    teza = res[0].weight;
        			    	var results = "<table class='table table-striped " +
                "table-hover'>";
        			        
        			results += "<tr><td class='text-left'>Teza</td><td class='text-middle'>" + res[0].weight +
                      " " + res[0].unit + "</td><td class='text-right'>" + res[0].time +
                      "</td></tr>";
        			        
        			        results += "</table>";
        			        $("#rezultatMeritveVitalnihZnakov").append(results);
        		    	} else {
        		    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-warning fade-in'>" +
                "Ni podatkov!</span>");
        		    	}
        		    },
        		    error: function() {
        		    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
        		    }
        		});
		
}

function kreirajEHRzaOsebo() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	

	if (!ehrId || ehrId.trim().length == 0 || !datumInUra || !telesnaVisina || !telesnaTeza) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite vse podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

$(document).ready(function() {
    $('#obstojeciEHR').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
});

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
